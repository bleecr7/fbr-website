/* jshint esversion:8 */
import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

import Home from './views/Home.vue';
const About = () => import(/* webpackChunkName: "about" */ './views/About.vue');
const Team = () => import(/* webpackChunkName: "team" */ './views/Team.vue');
const Cars = () => import(/* webpackChunkName: "cars" */ './views/Cars.vue');
// const News = () => import(/* webpackChunkName: "news" */ './views/News.vue');
// const Socials = () => import(/* webpackChunkName: "socials" */ './views/Socials.vue');
const Partners = () => import(/* webpackChunkName: "partners" */ './views/Partners.vue');
const Contact = () => import(/* webpackChunkName: "contact" */ './views/Contact.vue');
// const Success = () => import(/* webpackChunkName: "success" */ './views/Success.vue');
// const Member = () => import(/* webpackChunkName: "member" */ './views/Member.vue');
// const Login = () => import(/* webpackChunkName: "login" */ './views/Login.vue');
const ErrorPage = () => import(/* webpackChunkName: "error" */ './views/Error.vue');

export default new VueRouter({
    mode: 'history',
    base: (process.env.NODE_ENV !== 'development') ? process.env.BASE_URL : "/FBR/fbr-website/dist",
    routes: [
        // Main Pages
        {path: '/', name: 'home', component: Home, props: true},
        {path: '/about', name: 'about', component: About, props: true},
        {path: '/team', name: 'team', component: Team, props: true},
        {path: '/cars', name: 'cars', component: Cars, props: true},
        // {path: '/news', name: 'news', component: News, props: true},
        // {path: '/socials', name: 'socials', component: Socials, props: true},
        {path: '/partners', name: 'partners', component: Partners, props: true},
        {path: '/contact', name: 'contact', component: Contact, props: true},
        // {path: '/success', name: 'success', component: Success, props: true},
        // {path: '/login', name: 'login', component: Login, props: true},
        // {path: '/member', name: 'member', component: Member, props: true},
        {path: '*', name:'ErrorPage', component: ErrorPage, props: true},

        // Sub Pages (e.g. blog posts, car info etc)
    ],
    scrollBehavior(to, from, savedPosition) {
        return {
            x: 0,
            y: 0
        };
    }
});