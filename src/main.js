/*jshint esversion: 8 */
import Vue from 'vue';
import App from './App.vue';
import router from './router.js';

Vue.config.productionTip = false;

export const serverBus = new Vue();

export let vm = new Vue({
    router,
    render: h => h(App),
}).$mount('#app');
