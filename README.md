# FBR-Website

[![Netlify Status](https://api.netlify.com/api/v1/badges/6423bd9a-3580-4ab8-a6e6-286fa71fd0ff/deploy-status)](https://app.netlify.com/sites/fullblueracing/deploys)

[Live Site Link](https://fullblueracing.co.uk)

## Setup

1. Clone repository - [Git SSH setup instruction](https://docs.gitlab.com/ee/ssh/)

2. Install dependencies

    ```bash
    npm install package.json
    ```

3. After making and saving changes (in `src`!):

    ```bash
    npm start
    ```

    `webpack` will output updated files in `dist/`

Make sure to run `npm run build` before each commit to make sure it will still build in production setup!

## Testing

Set up a server of some sort - I use Apache, but as long as you can get it on localhost anything works.

## Resources

[HTML tutorial](https://www.w3schools.com/html/)

[SCSS tutorial](https://www.toptal.com/sass/theming-scss-tutorial)

[Vue.js Intro](https://vuejs.org/v2/guide/)

[CUER Template original git repo](https://github.com/yann-soubeyrand/hugo-spectral-theme)

[Minimal example of HTML, CSS, and JS](https://danby.soc.srcf.net/news/)
