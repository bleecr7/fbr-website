/* jshint esversion:8 */
const webpack = require("webpack");
const package = require("./package.json");
const VueLoaderPlugin = require("vue-loader/lib/plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const path = require('path');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
    .BundleAnalyzerPlugin;

const isDevelopment = process.env.NODE_ENV === 'development';

module.exports = {
    optimization: {
        noEmitOnErrors: true,
        runtimeChunk: 'single',
        splitChunks: {
            chunks: 'all',
            maxInitialRequests: Infinity,
            minSize: 0,
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name(module) {
                        // get the name. E.g. node_modules/packageName/not/this/part.js
                        // or node_modules/packageName
                        const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];

                        // npm package names are URL-safe, but some servers don't like @ symbols
                        return `npm.${packageName.replace('@', '')}`;
                    },
                },
            },
        },
        minimize: true,
        minimizer: [
            new TerserPlugin({
                cache: true,
                parallel: true,
                sourceMap: true, // Must be set to true if using source-maps in production
                terserOptions: {
                    ecma: undefined,
                    warnings: false,
                    parse: {},
                    compress: {},
                    mangle: true, // Note `mangle.properties` is `false` by default.
                    module: false,
                    output: null,
                    toplevel: false,
                    nameCache: null,
                    ie8: false,
                    keep_classnames: undefined,
                    keep_fnames: false,
                    safari10: false,
                }
            }),
        ],
    },
    mode: "development",
    devtool: 'inline-source-map',
    entry: ["./src/main.js", "./src/router.js"],
    output: {
        filename: '[name].js',
    },
    module: {
        rules: [{
                test: /\.vue$/,
                include: /src/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        'scss': [
                            'vue-style-loader',
                            'sass-loader',
                            'css-loader',
                        ],
                        'css': [
                            'vue-style-loader',
                            'css-loader',
                            'sass-loader'
                        ],
                        'sass': [
                            'vue-style-loader',
                            'css-loader',
                            'sass-loader?indentedSyntax'
                        ]
                    }
                }
            },
            {
                test: /\.(png|svg|jpg|gif|ttf)$/,
                loader: "file-loader?name=/public/[name].[ext]"
            },
            {
                test: /\.(txt)$/,
                loader: "raw-loader?name=/public/[name].[ext]"
            },
            {
                test: /\.s(a|c)ss$/,
                exclude: /\.module.(s(a|c)ss)$/,
                loader: [
                    isDevelopment ? 'style-loader' : {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: './'
                        }
                    },
                    'css-loader',
                    'resolve-url-loader',
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: isDevelopment
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                loader: ['css-loader']
            },
        ]
    },
    resolve: {
        extensions: [".js", ".ts", ".vue", ".css", ".scss"],
    },
    plugins: [
        // new BundleAnalyzerPlugin(),
        new webpack.HashedModuleIdsPlugin(),
        new VueLoaderPlugin(),
        new webpack.LoaderOptionsPlugin({
            minimize: true
        }),
        new HtmlWebpackPlugin({
            hash: true,
            title: 'TTP Full Blue Racing',
            template: './src/index.html',
            filename: './index.html' //relative to root of the application
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css'
        }),
    ]
};